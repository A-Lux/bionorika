<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Pagination Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the paginator library to build
    | the simple pagination links. You are free to change them to anything
    | you want to customize your views to better match your application.
    |
    */

    'question is required'                          => 'Вопрос :number обязательно для заполнения',
    'answer is required'                            => 'Ответ для :number вопроса обязателен',
    'soon the results will be come out'             => 'Ваши ответы отправлены на рассмотрение модераторам',
    'ready'                                         => 'Спасибо, ',
    'your score'                                    => 'Вы набрали :correct балла(ов) из :quantity',
    'not available'                                 => 'Недоступен',
    'attempt'                                       => 'Попытка №',
    'not used'                                      => 'Не использована',
    'An email has been sent to you!'                => 'Вам отправлено письмо на почту!',
    'Invalid code'                                  => 'Не правильный код!',
    'Search result found'                           => 'Результаты поиска по запросу :keyword',
    'Search result not found'                       => 'По запросу :keyword ничего не найдено',
    'video order'                                   => 'Видеолекция №:num',
    'Неправильный логин или пароль'                 => 'Неправильный логин или пароль.',
    'Ваша заявка успешно отправлена'                => 'Ваша заявка успешно отправлена!',
    'Январь - Апрель'                               => 'Январь - Апрель',
    'Май - Август'                                  => 'Май - Август',
    'Сентябрь - Декабрь'                            => 'Сентябрь - Декабрь',
    'цикл'                                          => 'цикл',
    'Общее количество фитобаллов на текущий момент' => 'Общее количество фитобаллов на текущий момент :count',
    'Максимальное количество фитобаллов'            => 'Максимальное количество фитобаллов :count'
];

<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/login_403', function (){
    return response('unauthorized', 403);
})->name('login');


Route::group(['prefix' => 'auth'], function () {
    Route::post('/login', 'Auth\LoginController@login');
    Route::post('/register', 'Auth\RegisterController@register');
    Route::get('/forgot-password', 'Auth\ForgotPasswordController@sendCode');
    Route::get('/code', 'Auth\ForgotPasswordController@checkCode');
});


Route::group(['prefix' => 'users'], function () {
    Route::apiResource('professions', '\App\Http\Controllers\Api\ProfessionAPIController', ['only' => ['index', 'show']]);
    Route::apiResource('directions', '\App\Http\Controllers\Api\DirectionAPIController', ['only' => ['index', 'show']]);
    Route::apiResource('interests', '\App\Http\Controllers\Api\InterestAPIController', ['only' => ['index', 'show']]);
    Route::apiResource('news', '\App\Http\Controllers\Api\NewsAPIController', ['only' => ['index', 'show']]);
    Route::apiResource('catalogs', '\App\Http\Controllers\Api\CatalogAPIController', ['only' => ['index']]);
    Route::apiResource('products', '\App\Http\Controllers\Api\ProductAPIController', ['only' => ['index', 'show']]);
    Route::apiResource('banner', '\App\Http\Controllers\Api\BannerAPIController', ['only' => ['index']]);
    Route::apiResource('about', '\App\Http\Controllers\Api\AboutAPIController', ['only' => ['index', 'show']]);
    Route::get('logotype', '\App\Http\Controllers\Api\LogotypeCopyrightAPIController@logotype', ['only' => ['logotype']]);
    Route::get('copyright', '\App\Http\Controllers\Api\LogotypeCopyrightAPIController@copyright', ['only' => ['copyright']]);
    Route::get('menu', '\App\Http\Controllers\Api\MenuAPIController@menu', ['only' => ['menu']]);
    Route::get('metaTag', '\App\Http\Controllers\Api\MenuAPIController@metaTag', ['only' => ['metaTag']]);
    Route::get('translate', '\App\Http\Controllers\Api\TranslationAPIController@translate', ['only' => ['translate']]);
    Route::post('search', '\App\Http\Controllers\Api\SearchAPIController@search', ['only' => ['search']]);
    Route::apiResource('faq', '\App\Http\Controllers\Api\FrequentlyAskedQuestionAPIController', ['only' => ['index', 'show']]);
    Route::apiResource('feedback', '\App\Http\Controllers\Api\FeedbackAPIController', ['only' => ['store']]);
    Route::apiResource('advertisements', '\App\Http\Controllers\Api\AdvertisementAPIController', ['only' => ['index']]);
    Route::apiResource('farmanadzor', '\App\Http\Controllers\Api\PharmacovigilanceAPIController', ['only' => ['index']]);

    Route::group(['middleware' => 'auth:api'], function () {
        Route::apiResource('questions', '\App\Http\Controllers\Api\QuestionAPIController', ['only' => ['index', 'show']]);
        Route::post('/logout', 'Auth\LoginController@logout');
        Route::post('/answers', 'Auth\AnswerController@answer');
        Route::apiResource('profile', '\App\Http\Controllers\Api\UserAPIController', ['only' => ['index', 'update']]);

        Route::apiResource('video-lessons', '\App\Http\Controllers\Api\VideoLessonAPIController', ['only' => ['index']]);
        Route::get('/watched-video-lessons', '\App\Http\Controllers\Api\WatchedVideoAPIController@index');
        Route::put('/watched-video-lessons/update', '\App\Http\Controllers\Api\WatchedVideoAPIController@update');

        Route::post('/test/info', '\App\Http\Controllers\Api\TestAPIController@index');
        Route::post('/test/start', '\App\Http\Controllers\Api\TestAPIController@start');
        Route::post('/test/finish', '\App\Http\Controllers\Api\TestAPIController@finish');
        Route::get('/test/received-points', '\App\Http\Controllers\Api\TestAPIController@receivedPoints');
        Route::get('/test/passed-tests', '\App\Http\Controllers\Api\TestAPIController@passedTests');

        Route::apiResource('questionnaire', '\App\Http\Controllers\Api\QuestionnaireAPIController', ['only' => ['index','store']]);
        Route::apiResource('cup', '\App\Http\Controllers\Api\CupAPIController', ['only' => ['index']]);

        Route::get('/scale', '\App\Http\Controllers\Api\ScaleAPIController@index');
        Route::apiResource('instructions', '\App\Http\Controllers\Api\InstructionAPIController', ['only' => ['index']]);
        Route::apiResource('certificates', '\App\Http\Controllers\Api\CertificateAPIController', ['only' => ['index','store']]);
        Route::delete('/certificates', '\App\Http\Controllers\Api\CertificateAPIController@destroy');

    });
});




<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Facades\Voyager;
use TCG\Voyager\Traits\Translatable;


class PharmacistVideoLesson extends Model
{
    use Translatable;
    protected $translatable = ['name', 'description'];

    public static function getAll($arr = null){

        $model = self::where('is_active', 1);

        if(is_array($arr)) {
            $model = $model->whereIn('id', $arr);
        }

        $model = $model->select('id', 'name', 'background', 'description', 'video_lecture')
            ->orderBy('sort', 'ASC')->get();

        $m = 0;
        foreach ($model as $v){
            $m++;
            $v->title = trans('messages.video order', ['num' => $m]);
            $v->background = Voyager::image($v->background);
            $v->test_status = PharmacistTest::getStatus($v->id);
        }

        return $model;
    }


    public static function exists($lesson_id){
        return self::where([['id', $lesson_id], ['is_active', 1]])->exists();
    }
}

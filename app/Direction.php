<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Traits\Translatable;

class Direction extends Model
{

    use Translatable;
    protected $translatable = ['name'];

    public static function getAll(){
        return self::orderBy('sort', 'ASC')->select('id', 'name')->get();
    }

}

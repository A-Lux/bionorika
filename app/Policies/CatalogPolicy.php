<?php

namespace App\Policies;

use App\User;
use App\Catalog;
use Illuminate\Auth\Access\HandlesAuthorization;

class CatalogPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any catalog.
     *
     * @param  App\User  $user
     * @return bool
     */
    public function viewAny(User $user)
    {
        return false;
    }

    /**
     * Determine whether the user can view the catalog.
     *
     * @param  App\User  $user
     * @param  App\Catalog  $catalog
     * @return bool
     */
    public function view(User $user, Catalog $catalog)
    {
        return false;
    }

    /**
     * Determine whether the user can create a catalog.
     *
     * @param  App\User  $user
     * @return bool
     */
    public function create(User $user)
    {
        return false;
    }

    /**
     * Determine whether the user can update the catalog.
     *
     * @param  App\User  $user
     * @param  App\Catalog  $catalog
     * @return bool
     */
    public function update(User $user, Catalog $catalog)
    {
        return false;
    }

    /**
     * Determine whether the user can delete the catalog.
     *
     * @param  App\User  $user
     * @param  App\Catalog  $catalog
     * @return bool
     */
    public function delete(User $user, Catalog $catalog)
    {
        return false;
    }

    /**
     * Determine whether the user can restore the catalog.
     *
     * @param  App\User  $user
     * @param  App\Catalog  $catalog
     * @return bool
     */
    public function restore(User $user, Catalog $catalog)
    {
        return false;
    }

    /**
     * Determine whether the user can permanently delete the catalog.
     *
     * @param  App\User  $user
     * @param  App\Catalog  $catalog
     * @return bool
     */
    public function forceDelete(User $user, Catalog $catalog)
    {
        return false;
    }
}

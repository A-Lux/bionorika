<?php

namespace App\Policies;

use App\User;
use App\Profession;
use Illuminate\Auth\Access\HandlesAuthorization;

class ProfessionPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any profession.
     *
     * @param  App\User  $user
     * @return bool
     */
    public function viewAny(User $user)
    {
        return false;
    }

    /**
     * Determine whether the user can view the profession.
     *
     * @param  App\User  $user
     * @param  App\Profession  $profession
     * @return bool
     */
    public function view(User $user, Profession $profession)
    {
        return false;
    }

    /**
     * Determine whether the user can create a profession.
     *
     * @param  App\User  $user
     * @return bool
     */
    public function create(User $user)
    {
        return false;
    }

    /**
     * Determine whether the user can update the profession.
     *
     * @param  App\User  $user
     * @param  App\Profession  $profession
     * @return bool
     */
    public function update(User $user, Profession $profession)
    {
        return false;
    }

    /**
     * Determine whether the user can delete the profession.
     *
     * @param  App\User  $user
     * @param  App\Profession  $profession
     * @return bool
     */
    public function delete(User $user, Profession $profession)
    {
        return false;
    }

    /**
     * Determine whether the user can restore the profession.
     *
     * @param  App\User  $user
     * @param  App\Profession  $profession
     * @return bool
     */
    public function restore(User $user, Profession $profession)
    {
        return false;
    }

    /**
     * Determine whether the user can permanently delete the profession.
     *
     * @param  App\User  $user
     * @param  App\Profession  $profession
     * @return bool
     */
    public function forceDelete(User $user, Profession $profession)
    {
        return false;
    }
}

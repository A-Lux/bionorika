<?php

namespace App\Policies;

use App\User;
use App\LogotypeCopyright;
use Illuminate\Auth\Access\HandlesAuthorization;

class LogotypeCopyrightPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any logotypeCopyright.
     *
     * @param  App\User  $user
     * @return bool
     */
    public function viewAny(User $user)
    {
        return false;
    }

    /**
     * Determine whether the user can view the logotypeCopyright.
     *
     * @param  App\User  $user
     * @param  App\LogotypeCopyright  $logotypeCopyright
     * @return bool
     */
    public function view(User $user, LogotypeCopyright $logotypeCopyright)
    {
        return false;
    }

    /**
     * Determine whether the user can create a logotypeCopyright.
     *
     * @param  App\User  $user
     * @return bool
     */
    public function create(User $user)
    {
        return false;
    }

    /**
     * Determine whether the user can update the logotypeCopyright.
     *
     * @param  App\User  $user
     * @param  App\LogotypeCopyright  $logotypeCopyright
     * @return bool
     */
    public function update(User $user, LogotypeCopyright $logotypeCopyright)
    {
        return false;
    }

    /**
     * Determine whether the user can delete the logotypeCopyright.
     *
     * @param  App\User  $user
     * @param  App\LogotypeCopyright  $logotypeCopyright
     * @return bool
     */
    public function delete(User $user, LogotypeCopyright $logotypeCopyright)
    {
        return false;
    }

    /**
     * Determine whether the user can restore the logotypeCopyright.
     *
     * @param  App\User  $user
     * @param  App\LogotypeCopyright  $logotypeCopyright
     * @return bool
     */
    public function restore(User $user, LogotypeCopyright $logotypeCopyright)
    {
        return false;
    }

    /**
     * Determine whether the user can permanently delete the logotypeCopyright.
     *
     * @param  App\User  $user
     * @param  App\LogotypeCopyright  $logotypeCopyright
     * @return bool
     */
    public function forceDelete(User $user, LogotypeCopyright $logotypeCopyright)
    {
        return false;
    }
}

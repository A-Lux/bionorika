<?php

namespace App\Policies;

use App\User;
use App\Interest;
use Illuminate\Auth\Access\HandlesAuthorization;

class InterestPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any interest.
     *
     * @param  App\User  $user
     * @return bool
     */
    public function viewAny(User $user)
    {
        return false;
    }

    /**
     * Determine whether the user can view the interest.
     *
     * @param  App\User  $user
     * @param  App\Interest  $interest
     * @return bool
     */
    public function view(User $user, Interest $interest)
    {
        return false;
    }

    /**
     * Determine whether the user can create a interest.
     *
     * @param  App\User  $user
     * @return bool
     */
    public function create(User $user)
    {
        return false;
    }

    /**
     * Determine whether the user can update the interest.
     *
     * @param  App\User  $user
     * @param  App\Interest  $interest
     * @return bool
     */
    public function update(User $user, Interest $interest)
    {
        return false;
    }

    /**
     * Determine whether the user can delete the interest.
     *
     * @param  App\User  $user
     * @param  App\Interest  $interest
     * @return bool
     */
    public function delete(User $user, Interest $interest)
    {
        return false;
    }

    /**
     * Determine whether the user can restore the interest.
     *
     * @param  App\User  $user
     * @param  App\Interest  $interest
     * @return bool
     */
    public function restore(User $user, Interest $interest)
    {
        return false;
    }

    /**
     * Determine whether the user can permanently delete the interest.
     *
     * @param  App\User  $user
     * @param  App\Interest  $interest
     * @return bool
     */
    public function forceDelete(User $user, Interest $interest)
    {
        return false;
    }
}

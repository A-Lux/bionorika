<?php

namespace App\Policies;

use App\User;
use App\About;
use Illuminate\Auth\Access\HandlesAuthorization;

class AboutPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any about.
     *
     * @param  App\User  $user
     * @return bool
     */
    public function viewAny(User $user)
    {
        return false;
    }

    /**
     * Determine whether the user can view the about.
     *
     * @param  App\User  $user
     * @param  App\About  $about
     * @return bool
     */
    public function view(User $user, About $about)
    {
        return false;
    }

    /**
     * Determine whether the user can create a about.
     *
     * @param  App\User  $user
     * @return bool
     */
    public function create(User $user)
    {
        return false;
    }

    /**
     * Determine whether the user can update the about.
     *
     * @param  App\User  $user
     * @param  App\About  $about
     * @return bool
     */
    public function update(User $user, About $about)
    {
        return false;
    }

    /**
     * Determine whether the user can delete the about.
     *
     * @param  App\User  $user
     * @param  App\About  $about
     * @return bool
     */
    public function delete(User $user, About $about)
    {
        return false;
    }

    /**
     * Determine whether the user can restore the about.
     *
     * @param  App\User  $user
     * @param  App\About  $about
     * @return bool
     */
    public function restore(User $user, About $about)
    {
        return false;
    }

    /**
     * Determine whether the user can permanently delete the about.
     *
     * @param  App\User  $user
     * @param  App\About  $about
     * @return bool
     */
    public function forceDelete(User $user, About $about)
    {
        return false;
    }
}

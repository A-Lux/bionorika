<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class PharmacistScoreType extends Model
{
    protected $fillable = [
        'score_id', 'question_id', 'answer_id', 'own_answer', 'is_correct'
    ];

    public function exists($score_id, $question_id){
        return self::where([['score_id', $score_id],['question_id', $question_id]])->exists();
    }



    public function createNew($score_id, $question_id, $answer_id, $is_correct){
        if(!$this->exists($score_id, $question_id)){
            return self::create([
                'score_id' => $score_id,
                'question_id' => $question_id,
                'answer_id' => $answer_id,
                'is_correct' => $is_correct
            ]);
        }
    }



    public function question(){
        return $this->belongsTo('App\PharmacistTestQuestion', 'question_id', 'id');
    }

    public function questionDescription(){
        return $this->question ? $this->question->description : "Не указано";
    }

    public function answer(){

        $answer = 'Не указано';
        if($this->question){
            if($this->answer_id == 1) $answer = $this->question->first_option;
            elseif($this->answer_id == 2) $answer = $this->question->second_option;
            elseif($this->answer_id == 3) $answer = $this->question->third_option;
            elseif($this->answer_id == 4) $answer = $this->question->fourth_option;
        }

        return $answer;
    }


    public static function getAllByScoreID($score_id){
        return self::where('score_id', $score_id)->get();
    }


    public static function check($score_id){
        return self::where("score_id", $score_id)->exists();
    }



    public static function getCorrectCount($score_id){
        return self::where(['score_id' => $score_id, 'is_correct' => 1])->count();
    }


}

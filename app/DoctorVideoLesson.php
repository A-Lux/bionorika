<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Facades\Voyager;
use TCG\Voyager\Traits\Translatable;


class DoctorVideoLesson extends Model
{
    use Translatable;
    protected $translatable = ['name', 'description'];

    public static function getAllByDirection($direction_id, $arr = null){

        $model = self::join('doctor_video_lesson_directions',  'doctor_video_lessons.id','=',
            'doctor_video_lesson_directions.doctor_video_lesson_id')
            ->where(['doctor_video_lessons.is_active' => 1, 'doctor_video_lesson_directions.direction_id' => $direction_id]);

        if(is_array($arr)) {
            $model = $model->whereIn('doctor_video_lessons.id', $arr);
        }

        $model = $model
            ->select('doctor_video_lessons.id' ,'doctor_video_lessons.name', 'doctor_video_lessons.background',
                'doctor_video_lessons.description', 'doctor_video_lessons.video_lecture')
            ->orderBy('doctor_video_lessons.sort', 'ASC')
            ->get();

        $m = 0;
        foreach ($model as $v){
            $m++;
            $v->title = trans('messages.video order', ['num' => $m]);
            $v->background = Voyager::image($v->background);
            $v->test_status = DoctorTest::getStatus($v->id);
        }

        return $model;
    }



    public static function exists($lesson_id){
        return self::where([['id', $lesson_id], ['is_active', 1]])->exists();
    }
}

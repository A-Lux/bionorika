<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;


class UserProfile extends Model
{

    const isDoctor = 2;
    const isPharmacist = 1;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'profession_id', 'direction_id', 'interest_id', 'city', 'phone', 'user_id', 'place_of_work', 'first_password'
    ];

    public static function getProfileByID($id){
        return self::where('user_id', $id)->first();
    }

    public static function getAll(){
        return self::get()->keyBy('user_id');
    }

    public function createNew($input){
        return self::create($input);;
    }

    public function user(){
        return $this->hasOne('App\User', 'id', 'user_id');
    }

    public function profession(){
        return $this->hasOne('App\Profession', 'id', 'profession_id');
    }

    public function interest(){
        return $this->hasOne('App\Interest', 'id', 'interest_id');
    }

    public function direction(){
        return $this->hasOne('App\Direction', 'id', 'direction_id');
    }

    public function professionName(){
        return $this->profession ? $this->profession->name : "Не указано";
    }

    public function interestName(){
        return $this->interest ? $this->interest->name : $this->interest_id;
    }

    public function directionName(){
        return $this->direction ? $this->direction->name : "";
    }


}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Facades\Voyager;


class Advertisement extends Model
{
    public static function getAll(){
        $model = self::select('id', 'image', 'url')->orderBy('sort', 'ASC')->get();
        foreach ($model as $v){
            $v->image = Voyager::image($v->image);
        }

        return $model;
    }
}

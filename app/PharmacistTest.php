<?php

namespace App;

use App\Traits\Testable;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Jenssegers\Date\Date;
use TCG\Voyager\Traits\Translatable;

class PharmacistTest extends Model
{
    use Translatable;
    use Testable;

    protected $translatable = ['name', 'theme'];

    const isActive = 1;
    const isNotActive = 0;
    const maxBall = 30;
    const firstCycleBall = 10;
    const secondCycleBall = 10;
    const thirdCycleBall = 10;

    public static function getStatus($lesson_id)
    {
        if ($model = self::where('pharmacist_video_lessont_id', $lesson_id)->first()){
            if (Carbon::createFromFormat('Y-m-d H:i:s' ,$model->from_time)->timestamp < Carbon::now()->timestamp &&
                Carbon::createFromFormat('Y-m-d H:i:s' ,$model->to_time)->timestamp > Carbon::now()->timestamp) {
                return self::isActive;
            } else return self::isNotActive;
        }else{
            return self::isNotActive;
        }
    }

    public static function getContent($user_id, $lesson_id){

        $model = self::where('pharmacist_video_lessont_id', $lesson_id)
            ->select('id' ,'name', 'theme', 'test_time', 'from_time', 'to_time', 'attempts')->first();
        $model->attempts_left = $model->attempts - PharmacistScore::getTestAttempt($user_id, $model->id);
        $model->test_time = Carbon::createFromFormat('H:i:s' ,$model->test_time)->minute.' минут';
        $model->number_of_questions = PharmacistTestQuestion::getCount($model->id);
        $model->test_is_valid = Date::parse($model->from_time)->format('j F Y г.').'  -  '.
            Date::parse($model->to_time)->format('j F Y г.');

        unset($model->from_time, $model->to_time);
        return $model;
    }


}

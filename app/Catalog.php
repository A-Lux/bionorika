<?php

namespace App;

use App\Helpers\TranslatesCollection;
use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Traits\Translatable;


class Catalog extends Model
{
    use Translatable;
    protected $translatable = ['name'];

    public static function getAll(){

        $model = self::whereNull('parent_id')
            ->orderBy('sort', 'ASC')
            ->with('categories')
            ->select('id', 'name')
            ->get();

        foreach ($model as $v){
            if($v->categories->count() > 0){
                $v->isActiveMenu = 1;
                $v->categories = self::getCategories($v->categories);
            }else{
                $v->isActiveMenu = 0;
//                $v->products = self::getProducts($v->products);
            }
        }

        return $model;
    }



    public static function search($keyword){

        if(app()->getLocale() == 'ru') {
            $model = self::where('name', 'LIKE', "%{$keyword}%")
                ->select('id', 'name')
                ->get();
        }else {
            $model = self::whereTranslation('name', 'LIKE', "%{$keyword}%", [app()->getLocale()], false)
                ->select('id', 'name')
                ->get();
        }

        return $model;
    }



    protected static function getCategories($categories){

        TranslatesCollection::translate($categories, app()->getLocale());

        foreach ($categories as $category){
            unset($category['parent_id'], $category['created_at'], $category['updated_at'], $category['sort']);
//            $category->products = self::getProducts($category->products);
        }

        return $categories;
    }



    protected static function getProducts($products){

        TranslatesCollection::translate($products, app()->getLocale());

        foreach ($products as $product){
            unset($product['catalog_id'], $product['created_at'], $product['updated_at'], $product['sort']);
        }

        return $products;
    }





    public function categories(){
        return $this->hasMany('App\Catalog', 'parent_id', 'id')
            ->orderBy('sort', 'ASC');
    }


    public function products(){
        return $this->hasMany('App\Product', 'catalog_id', 'id');
    }
}

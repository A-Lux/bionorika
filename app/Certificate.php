<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;


class Certificate extends Model
{

    const isActive = 1;
    const isNotActive = 0;

    protected $fillable = ['user_id', 'year', 'month', 'status', 'certificate'];

    public function createNew($user_id, $download_link, $original_name){

        $newFile = [["download_link" => $download_link,"original_name" => $original_name]];
        return self::create([
            'user_id' => $user_id,
            'year' => Carbon::now()->year,
            'month' => Carbon::now()->month,
            'status' => self::isNotActive,
            'certificate' => json_encode($newFile)
        ]);

    }


    public function addNewFile($user_id, $download_link, $original_name){

        $newFile = ["download_link" => $download_link,"original_name" => $original_name];
        $files = json_decode($this->certificate);
        $files[] = $newFile;

        return self::update([
            'user_id' => $user_id,
            'status' => self::isNotActive,
            'certificate' => json_encode($files)
        ]);
    }



    public static function getAll($months){
        return self::where(['year' => Carbon::now()->year])
            ->whereIn('month', $months)
            ->get()->keyBy('user_id');
    }

    public static function getByUser($user_id, $months){
        return self::where(['user_id' => $user_id, 'year' => Carbon::now()->year])
            ->whereIn('month', $months)
            ->first();
    }

    public static function getByUserYearAndMonth($user_id, $year, $months){
        return self::where(['user_id' => $user_id, 'year' => $year])
            ->whereIn('month', $months)
            ->first();
    }



}

<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 02.06.2020
 * Time: 17:24
 */

namespace App\Traits;


use Carbon\Carbon;

trait Cyclable
{

    public function getMonths(){

        $month = [
            [1,2,3,4],
            [5,6,7,8],
            [9,10,11,12]
        ];

        return $month;
    }


    public function getMonthsAsArray(){

        $month = $this->getMonths();
        $result = [];

        for ($i = 0;$i < 3;$i++){
            if(in_array(Carbon::now()->month, $month[$i])){
                $result = $month[$i];
                break;
            }
        }

        return $result;
    }


    public function getCurrentCycle(){

        $month = $this->getMonths();
        $result = [];

        for ($i = 0;$i < 3;$i++){
            if(in_array(Carbon::now()->month, $month[$i])){
                $result = $i+1;
                break;
            }
        }

        return $result;
    }


    public function getFirstCycleMonthsAsArray(){
        return [1,2,3,4];
    }
}

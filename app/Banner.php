<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Facades\Voyager;
use TCG\Voyager\Traits\Translatable;


class Banner extends Model
{
    use Translatable;
    protected $translatable = ['title', 'content'];

    public static function getAll(){

        $model = Banner::select('id', 'title', 'content')
            ->orderBy('sort', 'ASC')
            ->get();

        foreach($model as $v){

            $videos = BannerFile::getAllByBanner($v->id);

            foreach ($videos as $video){
                if($video->type == 1){
                    $video->type = 'video';
                    unset($video->image);
                }else{
                    $video->type = 'image';
                    unset($video->video);
                    if($video->image != null) {
                        $video->image = Voyager::image($video->image);
                    }
                }

                unset($video->banner_id);
            }

            $v->files = $videos;
        }

        return $model;
    }
}

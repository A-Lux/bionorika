<?php

namespace App;

use App\Helpers\TranslatesCollection;
use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Traits\Translatable;


class DoctorTestQuestion extends Model
{
    use Translatable;
    protected $translatable = ['description', 'first_option', 'second_option', 'third_option'];

    public static function getCount($test_id){
        return self::where('test_id', $test_id)->count();
    }

    public static function getQuestions($test_id){

        $model = self::where('test_id', $test_id)
            ->select('id','description', 'first_option', 'second_option', 'third_option')->get();
        TranslatesCollection::translate($model, app()->getLocale());

        $m = 0;
        foreach ($model as $v){

            $m++;
            $v['order'] = $m;

            $v->answers = [
                ['id' => 1, 'text' => $v->first_option],
                ['id' => 2, 'text' => $v->second_option],
                ['id' => 3, 'text' => $v->third_option],
            ];

            unset($v['first_option'], $v['second_option'], $v['third_option']);
        }

        return $model;
    }


}

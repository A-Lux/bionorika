<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 04.06.2020
 * Time: 9:55
 */

namespace App\Http\Controllers\Admin;


use Illuminate\Http\Request;
use TCG\Voyager\Http\Controllers\VoyagerBaseController;

class InstructionController extends VoyagerBaseController
{
    public function index(Request $request){
        return parent::show($request, 1);
    }

}

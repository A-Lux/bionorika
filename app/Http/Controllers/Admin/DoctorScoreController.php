<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 13.04.2020
 * Time: 19:43
 */

namespace App\Http\Controllers\Admin;


use App\DoctorScore;
use App\DoctorScoreType;
use App\DoctorTest;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use TCG\Voyager\Facades\Voyager;
use TCG\Voyager\Http\Controllers\VoyagerBaseController;

class DoctorScoreController extends VoyagerBaseController
{

    public function show(Request $request, $id)
    {
        $slug = $this->getSlug($request);

        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        $isSoftDeleted = false;

        if (strlen($dataType->model_name) != 0) {
            $model = app($dataType->model_name);

            // Use withTrashed() if model uses SoftDeletes and if toggle is selected
            if ($model && in_array(SoftDeletes::class, class_uses_recursive($model))) {
                $model = $model->withTrashed();
            }
            if ($dataType->scope && $dataType->scope != '' && method_exists($model, 'scope'.ucfirst($dataType->scope))) {
                $model = $model->{$dataType->scope}();
            }
            $dataTypeContent = call_user_func([$model, 'findOrFail'], $id);
            if ($dataTypeContent->deleted_at) {
                $isSoftDeleted = true;
            }
        } else {
            // If Model doest exist, get data from table name
            $dataTypeContent = DB::table($dataType->name)->where('id', $id)->first();
        }

        // Replace relationships' keys for labels and create READ links if a slug is provided.
        $dataTypeContent = $this->resolveRelations($dataTypeContent, $dataType, true);

        // If a column has a relationship associated with it, we do not want to show that field
        $this->removeRelationshipField($dataType, 'read');

        // Check permission
        $this->authorize('read', $dataTypeContent);

        // Check if BREAD is Translatable
        $isModelTranslatable = is_bread_translatable($dataTypeContent);

        // Eagerload Relations
        $this->eagerLoadRelations($dataTypeContent, $dataType, 'read', $isModelTranslatable);

        $view = 'voyager::bread.read';

        if (view()->exists("voyager::$slug.read")) {
            $view = "voyager::$slug.read";
        }


        $answers = DoctorScoreType::where('score_id', $id)->get();
        $test = DoctorTest::find($dataTypeContent->test_id);
        $status = DoctorScore::checkStatus($id, $test);

        return Voyager::view($view, compact('answers' ,'dataType', 'dataTypeContent', 'isModelTranslatable', 'isSoftDeleted', 'answers', 'status'));
    }




    public function edit(Request $request, $id)
    {
        $slug = $this->getSlug($request);

        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        if (strlen($dataType->model_name) != 0) {
            $model = app($dataType->model_name);

            // Use withTrashed() if model uses SoftDeletes and if toggle is selected
            if ($model && in_array(SoftDeletes::class, class_uses_recursive($model))) {
                $model = $model->withTrashed();
            }
            if ($dataType->scope && $dataType->scope != '' && method_exists($model, 'scope'.ucfirst($dataType->scope))) {
                $model = $model->{$dataType->scope}();
            }
            $dataTypeContent = call_user_func([$model, 'findOrFail'], $id);
        } else {
            // If Model doest exist, get data from table name
            $dataTypeContent = DB::table($dataType->name)->where('id', $id)->first();
        }

        foreach ($dataType->editRows as $key => $row) {
            $dataType->editRows[$key]['col_width'] = isset($row->details->width) ? $row->details->width : 100;
        }

        // If a column has a relationship associated with it, we do not want to show that field
        $this->removeRelationshipField($dataType, 'edit');

        // Check permission
        $this->authorize('edit', $dataTypeContent);

        // Check if BREAD is Translatable
        $isModelTranslatable = is_bread_translatable($dataTypeContent);

        // Eagerload Relations
        $this->eagerLoadRelations($dataTypeContent, $dataType, 'edit', $isModelTranslatable);

        $view = 'voyager::bread.edit-add';

        if (view()->exists("voyager::$slug.edit-add")) {
            $view = "voyager::$slug.edit-add";
        }


        $answers = DoctorScoreType::where('score_id', $id)->get();

        return Voyager::view($view, compact('dataType', 'id' ,'dataTypeContent', 'isModelTranslatable', 'answers'));
    }



    public function updateAnswers(Request $request){

        if(Auth::user() != null && Auth::user()->role_id != 2 && Auth::user()->role_id != null){
            if($request->id != null){
                $answers = DoctorScoreType::getAllByScoreID($request->id);
                $score = 0;
                foreach($answers as $v){
                    $param =  "type_".$v->id;
                    if(isset($request->$param)){
                        $score++;
                        $v->update(['is_correct' => $request->$param]);
                    }else{
                        $v->update(['is_correct' => 0]);
                    }
                }

                DoctorScore::find($request->id)->update(['score' => $score]);
                return redirect('admin/doctor-scores/'.$request->id);
            }else{
                abort(404);
            }
        }else{
            return abort(404);
        }

    }

}

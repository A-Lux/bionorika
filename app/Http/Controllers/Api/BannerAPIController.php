<?php

namespace App\Http\Controllers\API;

use App\Banner;
use App\Helpers\TranslatesCollection;
use App\Http\Controllers\Controller;

class BannerAPIController extends Controller
{
    public function index()
    {
        $banner = Banner::getAll();
        TranslatesCollection::translate($banner, app()->getLocale());
        return response()->json(['data' => $banner], 200);
    }

}

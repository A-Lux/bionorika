<?php

namespace App\Http\Controllers\API;

use App\Helpers\TranslatesCollection;
use App\News;
use App\Http\Resources\NewsCollection;
use App\Http\Resources\NewsResource;
use App\Http\Controllers\Controller;

class NewsAPIController extends Controller
{
    public function index()
    {
        $news = News::getAll();
        TranslatesCollection::translate($news, app()->getLocale());
        return new NewsCollection($news);
    }
 
    public function show(News $news)
    {
        TranslatesCollection::translate($news, app()->getLocale());
        return new NewsResource($news);
    }

}

<?php

namespace App\Http\Controllers\API;

use App\Helpers\TranslatesCollection;
use App\Http\Resources\CopyrightResource;
use App\LogotypeCopyright;
use App\Http\Resources\LogotypeResource;
use App\Http\Controllers\Controller;

class LogotypeCopyrightAPIController extends Controller
{
    public function logotype()
    {
        $model = LogotypeCopyright::first();
        TranslatesCollection::translate($model, app()->getLocale());

        return new LogotypeResource($model);
    }
 
    public function copyright()
    {
        $model = LogotypeCopyright::first();
        TranslatesCollection::translate($model, app()->getLocale());

        return new CopyrightResource($model);
    }


}

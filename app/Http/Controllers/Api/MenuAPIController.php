<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 23.04.2020
 * Time: 18:37
 */

namespace App\Http\Controllers\API;
use App\Helpers\TranslatesCollection;
use App\Http\Controllers\Controller;
use App\Page;
use Illuminate\Http\Request;


class MenuAPIController extends Controller
{

    public function menu()
    {
        $menu = $this->getMenu();
        return response()->json(['data' => $menu], 200);
    }


    public function metaTag(Request $request){

        if(isset($request->url) ) $url = $request->url;
        else $url = null;

        $menu = $this->getMetaTag($url);

        return response()->json(['data' => $menu], 200);
    }


    private function getMenu(){

        $menus = menu('site', '_json');
        $data = [];

        foreach ($menus as $k => $v) {
            $menuItem = $v->translate(app()->getLocale());
            $data[] = [
                'title' => $menuItem->title,
                'url'   => $menuItem->url
            ];
        }

//        TranslatesCollection::translate($menus, app()->getLocale());
        return $data;
    }



    private function getMetaTag($url){

        $meta = Page::findByUrl($url); $menu = "";

        if($meta != null){
            TranslatesCollection::translate($meta, app()->getLocale());
        }

        return $meta;
    }
}


<?php

namespace App\Http\Controllers\API;

use App\DoctorQuestionnaire;
use App\UserProfile;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Api\TestAPIController;

class QuestionnaireAPIController extends TestAPIController
{

    public function index(Request $request){

        $user_id = Auth::id();
        $profile = UserProfile::getProfileByID($user_id);

        $validateArray = [
            'test_id'  => 'required'
        ];

        if($errors = $this->check($request, $validateArray)) {
            return $errors;
        }else{
            $test_id = $request->test_id;
        }

        if($profile->profession_id == UserProfile::isDoctor) {

            $data = $this->getDoctorQuestionnaire($user_id, $test_id);
            if($data != null){
                return response()->json(['data' => $data], 200);
            }
        }

        return response(['Network does not exist'], 422);
    }


    public function store(Request $request)
    {
        $user_id = Auth::id();
        $profile = UserProfile::getProfileByID($user_id);

        $validateArray = [
            'test_id'  => 'required'
        ];

        if($errors = $this->check($request, $validateArray)) {
            return $errors;
        }else{
            $answers = $request->answers;
            $test_id = $request->test_id;
        }

        if($profile->profession_id == UserProfile::isDoctor) {
            $questionnaire = $this->getDoctorQuestionnaire($user_id, $test_id);
            if($questionnaire != null){
                if($errors = $this->checkAndValidateQuestionnaire($answers, $questionnaire)) return $errors;
                foreach ($answers as $k => $v){
                    $userQuestion = new DoctorQuestionnaire();
                    $userQuestion->createNew($test_id, $user_id, $v['question_id'], $v['answer']);
                }

                $data = [];
                return response()->json(['data' => $data], 200);
            }
        }

        return response(['Network does not exist'], 422);
    }


    private function checkAndValidateQuestionnaire($answers, $questions){

        $errors = []; $m = 0;
        foreach ($questions as  $question) {

            $statusQuestion = 0; $statusAnswer = 0; $m++;
            if($answers != null) {
                foreach ($answers as $answer) {
                    if (isset($answer['question_id']) && $question->id == $answer['question_id']) {
                        $statusQuestion = 1;
                        if (isset($answer['answer'])) $statusAnswer = 1;
                    }
                }
            }

            if((!$statusQuestion && !$statusAnswer)) {
                $errors["error"][$question->id] = [
                    "question" => trans('messages.question is required', ['number' => $m]),
                    "answer" => trans('messages.answer is required', ['number' => $m])];
            }elseif(!$statusAnswer){
                $errors["error"][$question->id] = [
                    "answer" => trans('messages.answer is required', ['number' => $m])];
            }
        }

        return $errors;
    }

}

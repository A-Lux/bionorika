<?php

namespace App\Http\Controllers\API;

use App\Advertisement;
use App\Http\Resources\AdvertisementCollection;
use App\Http\Controllers\Controller;

class AdvertisementAPIController extends Controller
{
    public function index()
    {
        $advertisements = Advertisement::getAll();
        return new AdvertisementCollection($advertisements);
    }

}

<?php

namespace App\Http\Controllers\API;

use App\Helpers\TranslatesCollection;
use App\Profession;
use App\Http\Resources\ProfessionCollection;
use App\Http\Resources\ProfessionResource;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProfessionAPIController extends Controller
{
    public function index()
    {
        $profession = Profession::getAll();
        TranslatesCollection::translate($profession, app()->getLocale());
        return new ProfessionCollection($profession);
    }
 
    public function show(Profession $profession)
    {
        TranslatesCollection::translate($profession, app()->getLocale());
        return new ProfessionResource($profession);
    }


}

<?php

namespace App\Http\Controllers\API;

use App\Cup;
use App\UserProfile;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Api\ScaleAPIController;

class CupAPIController extends ScaleAPIController
{
    public function index()
    {
        $user_id = Auth::id();
        $profile = UserProfile::getProfileByID($user_id);
        $min_completion = Cup::getMinYearOfCompletion();
        $max_completion = Cup::getMaxYearOfCompletion();
        $completion = 0;
        $completions = [];
        $data = [];

        if($profile->profession_id == UserProfile::isDoctor){
            for($year = self::fromYear;$year <= Carbon::now()->year;$year++){
                $result = $this->checkDoctorCycleStatusAndGetData($user_id, $year);
                if($result["status"] == 1){
                    $completion++;
                }else{
                    $completions[] = $completion;
                    $completion = 0;
                }
            }

        }elseif($profile->profession_id == UserProfile::isPharmacist){
            for($year = self::fromYear;$year <= Carbon::now()->year;$year++){
                $result = $this->checkPharmacistCycleStatusAndGetData($user_id, $year);
                if($result["status"] == 1){
                    $completion++;
                }else{
                    $completions[] = $completion;
                    $completion = 0;
                }
            }

        }else{
            return response(['Network does not exist'], 422);
        }


        if(count($completions) > 0){
            $completion = max($completions);
        }

        if($min_completion <= $completion && $max_completion >= $completion){
            $data['status'] = 1;
            $data['cup'] = Cup::getByCompletion($completion);
        }elseif($max_completion < $completion){
            $data['status'] = 1;
            $data['cup'] = Cup::getByCompletion($max_completion);
        }else{
            $data['status'] = 0;
        }

        return response()->json(['data' => $data], 200);
    }

}

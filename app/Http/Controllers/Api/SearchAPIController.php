<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 27.04.2020
 * Time: 10:00
 */

namespace App\Http\Controllers\API;


use App\About;
use App\Catalog;
use App\Helpers\TranslatesCollection;
use App\Http\Controllers\Controller;
use App\News;
use App\Product;
use Illuminate\Http\Request;


class SearchAPIController extends Controller
{
    public function search(Request $request){

        $validateArray = ['keyword' => 'required'];

        if($errors = $this->check($request, $validateArray)) {
            return $errors;
        }

        $keyword = $request->keyword;
        $results = [];

        $products = Product::search($keyword);
        $catalogs = Catalog::search($keyword);
        $news = News::search($keyword);
        $about = About::search($keyword);

        if($products->count() > 0 || $catalogs->count() > 0 ||
            $news->count() > 0 || $about->count() > 0){

            if($products->count() > 0) {
                TranslatesCollection::translate($about, app()->getLocale());
            }

            if($catalogs->count() > 0) {
                TranslatesCollection::translate($whyOurCompany, app()->getLocale());
            }

            if($news->count() > 0) {
                TranslatesCollection::translate($findOutMore, app()->getLocale());
            }

            if($about->count() > 0){
                TranslatesCollection::translate($service_content, app()->getLocale());
            }

            $title = trans('messages.Search result found', ['keyword' => $keyword]);
            $status = 1;
        }else{
            $title = trans('messages.Search result not found', ['keyword' => $keyword]);
            $status = 0;
        }

        $results['products'] = $products;
        $results['catalogs'] = $catalogs;
        $results['news'] = $news;
        $results['about'] = $about;

        $data['title'] = $title;
        $data['status'] = $status;
        $data['results'] = $results;

        return response()->json(['data' => $data]);

    }
}

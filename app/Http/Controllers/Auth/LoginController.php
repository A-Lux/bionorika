<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    /**
     * login api
     *
     * @return \Illuminate\Http\Response
     */
    public function login(){
        if(Auth::attempt(['email' => request('email'), 'password' => request('password'), 'isActive' => 1])){
            $user = Auth::user();
            $token = $user->createToken('Laravel Password Grant Client')->accessToken;
            return response()->json(['token' => $token], 200);
        }
        else{
            return response()->json(['error' => trans('messages.Неправильный логин или пароль')], 401);
        }
    }



    public function logout()
    {
        Auth::guard('api')->user()->token()->revoke();
        return response([], 200);
    }
}

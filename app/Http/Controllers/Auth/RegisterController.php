<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use App\UserProfile;
use Illuminate\Http\Request;



class RegisterController extends Controller
{

    /**
     * Register api
     *
     * @return \Illuminate\Http\Response
     */



    public function register(Request $request)
    {
        $validateArray = [
            'name' => 'required|max:255|min:8',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|max:16|min:8',
            'password_confirmation' => 'required|same:password',
            'profession_id' => 'required|exists:professions,id',
            'interest_id'  => 'required|max:255',
            'city' => 'required|max:255',
            'phone' => 'required',
            'place_of_work' => 'required|max:255',
            'agreement' => 'accepted'
        ];

        $validateArray = $this->checkProfessionAndValidate($request, $validateArray);
        if($userValidate = $this->check($request, $validateArray)) {
            return $userValidate;
        }

        $input = $request->all();

        $input['password'] = bcrypt($request->password);
        $user = new User();
        $user = $user->createNew($input);

        $input['user_id'] = $user->id;
        $input['first_password'] = $request->password;
        $profile = new UserProfile();
        $profile->createNew($input);

        $token = $user->createToken('Laravel Password Grant Client')->accessToken;

        return response()->json(['token' => $token]);

    }



}
